app.controller('busquedaController' , function ($scope, $timeout, $route, $rootScope, $routeParams, $location, $http, Data, sessionService, CONFIG,sweet, LogGuardarInfo, DreamFactory) {

	var fecha= new Date();
	var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
	var fechactualnuevoformasto=fecha.getDate() + "/" + (fecha.getMonth() + 1) + "/" + fecha.getFullYear() + " - " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();	
	$scope.errors = {};
	$scope.prsCI = sessionService.get('USUARIO');
	$scope.idCiudadano = sessionService.get('IDSOLICITANTE');
	$scope.formNatural = true;
	$scope.derivarmostrar = true;
	var respuestaNexo = "";
	
	$scope.templates =
    [ 
        { name: 'template0.html', url: '../../../app/view/salud/sldHistoricoConsultasMenu/index.html'},
        { name: 'template1.html', url: '../../../app/view/nexo/formularioMostrar/index.html'},
        { name: 'template2.html', url: '../../../app/view/nexo/actuaciones/index.html'},
        { name: 'template3.html', url: '../../../app/view/nexo/fusion/index.html'},
        { name: 'template4.html', url: '../../../app/view/nexo/historico/index.html'},
        { name: 'template3.html', url: '../../../app/view/nexo/derivar/index.html'},
        { name: 'template6.html', url: '../../../app/view/salud/labResultados/index.html'},
        { name: 'template6.html', url: '../../../app/view/salud/docAgenda/index.html'}
       
    ];

    $scope.valor=function(opcion){
        $rootScope.verActuar=opcion;
        $rootScope.verTitulo=false;
    }
        
	$scope.cargar=function(cieId){
                if(cieId==1)
        {
            $rootScope.$broadcast('cargaFormulario', $scope.verActuar);
            //$scope.recuperarDatosBorrador();
        }
        if(cieId==3)
        {
            $rootScope.$broadcast('cargaFusiones', 'hola3');
        }
        if(cieId==4)
        {
            $rootScope.$broadcast('cargaHistorico', 'hola3');
        }
        $scope.template = $scope.templates[cieId];
    };

	$scope.inicioNexo = function(){
        var resRoles = {
            "procedure_name":"sp_lst_tipo_subtipo",
            "body":{
                "params": [
					{
					  "name": "id_tipo",
					  "value": null
					},
					{
					  "name": "accion",
					  "value": 1
					}
				  ]
            }
        };

        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resRoles);
        obj.success(function (response) {
            $scope.dataTipoTramites = response;           
        })
        obj.error(function(error) {
            $scope.errors["error_Documentos"] = error;            
        });      
    };

           /**$scope.recuperarDatosBorrador = function(){
        var tramiteuid=sessionService.get('TRAMITE_UID');
        var datosform = {};
        if(tramiteuid){
            var datosBorrador = {
                "procedure_name":"cargardatosborrador",
                "body":{
                    "params": [{ "name": "tramiteid", "value": sessionService.get('TRAMITE_UID')
                        },{ "name": "copianrocopia","value": sessionService.get('TRAMITE_NROCOPIA')
                    }]
                }
            };
            DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(datosBorrador).success(function (results){
                if(results.length > 0){
                    console.log('aquiiiii',results[0].copiadata);
                    datos = JSON.parse(results[0].copiadata);
                    $scope.datos = datos;
                    $scope.datosEstaticos = results[0];
                    
                } else {
                    sessionService.set('IDTRAMITE', sIdTramite);
                }
            }).error(function(results){
            });
        }else{
			alert( 'La sesión del tramite finalizao, vaya a Bandeja de entrada');
        }
    };*/
    
    
	$scope.subTipoTramites = function (tipoTramite) {
		var resRoles = {
            "procedure_name":"sp_lst_tipo_subtipo",
            "body":{
                "params": [
					{
					  "name": "id_tipo",
					  "value": tipoTramite
					},
					{
					  "name": "accion",
					  "value": 2
					}
				  ]
            }
        };
        $.blockUI();
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resRoles);
        obj.success(function (response) {
            $scope.dataSubTipoTramites = response;
             setTimeout(function(){            
                $.unblockUI();  $scope.DesbloquearMenu();
            },1000);
        })
        obj.error(function(error) {
            $scope.errors["error_Documentos"] = error;
            setTimeout(function(){            
                $.unblockUI();  $scope.DesbloquearMenu();
            },1000);
        });     
	};

	/****************************TATIANA **************************************/
    $scope.limpiar = function()
    {
    	$scope.tipoTramite = "";
    	$scope.subTipoTramite = "";
    	$scope.tipoHojaRuta = "";
    	$scope.asunto = "";
    	$scope.ubicacion = "";
    	$scope.zona ="";
        if($scope.datos != undefined) {
        $scope.datos.subTipoTramite=""; }
    	//$scope.checkboxes.items = "";
    	//$scope.checkboxes.items2 = "";
    	//$scope.checkboxes.items3 = "";
    	//$scope.checkboxes.items4 = "";
    	$scope.ubicacion ="";
    	$scope.nroformulario = "";
    	$scope.planilla="";
    };
    
      	
  	
 	$scope.buscarDatos = function (datos, rolesId) {        
    var datos1 = datos;
        var rolesId1 = rolesId;        
        if(datos1 === undefined)
        {
        //alert("datos es indefinido!");
         var misDatos = {
            "procedure_name":"sp_buscar_tramites",
            "body":{
                "params": [
					{
					  "name": "v_tipo_id",
					  "value": ''
					},
					{
					  "name": "v_suptipo_id",
					  "value": null
					},
					{
					  "name": "v_correlativo1",
					  "value": null
					},
                    {
					  "name": "v_correlativo2",
					  "value": null
					},
                    {
					  "name": "v_tipoHr",
					  "value": ''
					},
					{
					  "name": "v_asunto",
					  "value": ''
					},
					{
					  "name": "v_nodo_origen",
					  "value": rolesId
					},
					{
					  "name": "v_create_date1",
					  "value": null
					},
                    {
					  "name": "v_create_date2",
					  "value": null
					},
                    {
                      "name": "v_estado",
                      "value": ''
                    }
				  ]
            }
        };
        }
        
        /*********VALIDAR LOS CAMPOS PARA NO RECIBIR INDEFINIDO*********/
        else{
            var filtroUid1 = datos.app_uid1;
            var filtroUid2 = datos.app_uid2;
			var filtroTipoTramite = datos.tipoTramite;
            var filtroSubTipoTramite = datos.subTipoTramite;
			var filtroTipoHojaRuta = datos.tipoHojaRuta;
            var filtroAsunto = datos.asunto;
			var filtroEstado = datos.estado;
			var filtroFchDesde = datos.fchDesde;
            var filtroFchHasta = datos.fchHasta;
             console.log("este es el mensaje por hojas:" + filtroTipoHojaRuta);
            console.log("este es el mensaje por estado:" + filtroEstado);
            if(filtroUid1){
				var filtroUid1_v = datos.app_uid1;
				if(filtroUid2){ 
				var filtroUid2_v = datos.app_uid2;}
				else{var filtroUid2_v = datos.app_uid1;}
				}
            	else{var filtroUid1_v = null; var filtroUid2_V = null;}
			
			if(filtroTipoTramite){var filtroTipoTramite_v = datos.tipoTramite;}
            else{ var filtroTipoTramite_v = '';}
            
            if(filtroSubTipoTramite){var filtroSubTipoTramite_v = datos.subTipoTramite;}
            else{var filtroSubTipoTramite_v = null;}
			
			if(filtroTipoHojaRuta){ var filtroTipoHojaRuta_v = filtroTipoHojaRuta;}
            else{var filtroTipoHojaRuta_v = '';}
            
            if(filtroAsunto){ var filtroAsunto_v = datos.asunto;}
            else{var filtroAsunto_v = '';}
			
			if(filtroEstado){var filtroEstado_v = datos.estado;}
            else{ var filtroEstado_v = '';}
            
            if(filtroFchDesde){
			var filtroFchDesde_v = datos.fchDesde.getFullYear() + "-" + (datos.fchDesde.getMonth()+1) + "-" + datos.fchDesde.getDate();
			if(filtroFchHasta){ 
			var filtroFchHasta_v = datos.fchHasta.getFullYear() + "-" + (datos.fchHasta.getMonth()+1) + "-" + datos.fchHasta.getDate();}
			else{var filtroFchHasta_v = datos.fchDesde.getFullYear() + "-" + (datos.fchDesde.getMonth()+1) + "-" + datos.fchDesde.getDate();}
			}
            else{var filtroFchDesde_v = null; var filtroFchHasta_v = null;}
        }
        if(rolesId1 === undefined)
        {
            
            //alert("nodoid es indefinido!" + fechaCreacion1);
            var misDatos = {
            "procedure_name":"sp_buscar_tramites",
            "body":{
                 "params": [
					{
					  "name": "v_tipo_id",
					  "value": filtroTipoTramite_v
					},
					{
					  "name": "v_suptipo_id",
					  "value": filtroSubTipoTramite_v
					},
					{
					  "name": "v_correlativo1",
					  "value": filtroUid1_v
					},
                    {
					  "name": "v_correlativo2",
					  "value": filtroUid2_v
					},
                    {
					  "name": "v_tipoHr",
					  "value": filtroTipoHojaRuta_v
					},
					{
					  "name": "v_asunto",
					  "value": filtroAsunto_v
					},
					{
					  "name": "v_nodo_origen",
					  "value": null
					},
					{
					  "name": "v_create_date1",
					  "value": filtroFchDesde_v
					},
                    {
					  "name": "v_create_date2",
					  "value": filtroFchHasta_v
					},
                    {
                      "name": "v_estado",
                      "value": filtroEstado_v
                    }
				  ]
            }
        };
        }
        if(rolesId1 !== undefined && datos1 !== undefined)
        {
            //alert("ambos estan definidos");
            var misDatos = {
            "procedure_name":"sp_buscar_tramites",
            "body":{
                 "params": [
					{
					  "name": "v_tipo_id",
					  "value": filtroTipoTramite_v
					},
					{
					  "name": "v_suptipo_id",
					  "value": filtroSubTipoTramite_v
					},
					{
					  "name": "v_correlativo1",
					  "value": filtroUid1_v
					},
                    {
					  "name": "v_correlativo2",
					  "value": filtroUid2_v
					},
                    {
					  "name": "v_tipoHr",
					  "value": filtroTipoHojaRuta_v
					},
					{
					  "name": "v_asunto",
					  "value": filtroAsunto_v
					},
					{
					  "name": "v_nodo_origen",
					  "value": rolesId
					},
					{
					  "name": "v_create_date1",
					  "value": filtroFchDesde_v
					},
                    {
					  "name": "v_create_date2",
					  "value": filtroFchHasta_v
					},
                    {
                      "name": "v_estado",
                      "value": filtroEstado_v
                    }
				  ]
            }
        };
        }
        $.blockUI();
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(misDatos).success(function (response){
            if(response.length > 0){ 
                $scope.formFormulario = true;
                $scope.ResultadoBusqueda = true;
                $scope.tablaDatos = true;
                $scope.MostrarCopias = false;
                $scope.formActuacion = false;
                $scope.filtros = false;
                $scope.obtDatos = response;
                $scope.Recargar();
                //sweet.show('', "Datos Encontrados !!!", 'success');
                $.unblockUI(); 
            } else {
                sweet.show('', "No hay Registros con los criterios de busqueda", 'error');
                $scope.tablaDatos = false;
                $scope.Recargar();
                $.unblockUI(); 
            }
        }).error(function(response){
            $.unblockUI();             
            sweet.show('', "Error al realizar la consulta", 'error');
        });

    };
	

	$scope.habilitaActuacion = function(opcion, index,idNodo, correlativo, nroCopia, asunto, tipo_tramite)
    {  
        $.blockUI(); 
        switch(opcion) {
              case 1: // mostrar Tramite       
                    $scope.formActuacion = true;
                    $scope.formFormulario = false;
                    $scope.MostrarCopias = true;
					sessionService.set('TRAMITE_UID', index);
					sessionService.set('TRAMITE_IDNODO', idNodo);	
					sessionService.set('TRAMITE_NROCOPIA', nroCopia);
					if(nroCopia == 0){ nroCopia="Original";}
					$scope.titulo = correlativo+" - "+nroCopia+" |  Asunto: "+asunto+" | Tipo de Tramite:"+tipo_tramite;
					$.unblockUI(); 
                    break;
              case 2: // volver a bandeja de entrada
                    $scope.formActuacion = false;
                    $scope.formFormulario = true;
                    $scope.MostrarCopias = false;
					sessionService.destroy('TRAMITE_UID');
					sessionService.destroy('TRAMITE_IDNODO');
					sessionService.destroy('TRAMITE_NROCOPIA');
                    $.unblockUI(); 
                    break;
        }
    };
    
  $scope.verCopias = function (tramiteuid) {
         //alert('resultado '+ tramiteuid);
         $.blockUI();
         $scope.formActuacion = false;
         $scope.ResultadoBusqueda = false;
         $scope.formFormulario = true;
         $scope.MostrarCopias = true;
		 $scope.filtros = false;
                var datosCopia = {
                            "procedure_name":"sp_mostrar_hijos",
                            "body":{
                                  "params": [
                                    {
                                      "name": "prm_tramite_uid",
                                      "value": tramiteuid
                                    }
                                  ]
                            }                
                };        
                DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(datosCopia).success(function (response){
                                if(response.length > 0){
                                    
                                $scope.DataTable = response;
                                $.unblockUI(); 
                                } else {
                                sweet.show('', "No hay Registros con los criterios de busqueda", 'error');
                                $scope.tablaDatos = false;
                                $.unblockUI(); 
            }
        }).error(function(response){
        });

    };
    
	$scope.VerActuacion = function(opcion,uid, idNodo, correlativo, nroCopia, asunto, tipo)
   {   
         switch(opcion) {
              case 1: // mostrar Tramite 
                 //alert('opcion: ' + opcion + ' tramiteid: ' + uid + ' idNodo: ' +idNodo+ ' correlativo: ' + correlativo+ ' nroCopia: ' +nroCopia + ' Asunto: ' + asunto + 'tipotramite:' + tipo);
                    $scope.formActuacion = true;
                    $scope.formFormulario = true;
                    $scope.MostrarCopias = false;
					$scope.ResultadoBusqueda = false;
					$scope.filtros = false;
					sessionService.set('TRAMITE_UID', uid);
					sessionService.set('TRAMITE_IDNODO', idNodo);	
					sessionService.set('TRAMITE_NROCOPIA', nroCopia);
					if(nroCopia == 0){ nroCopia="Original";}
					//$scope.titulo = correlativo+" - "+nroCopia+" |  Asunto: "+asunto+" | Tipo de Tramite: "+tipo;
                    $scope.titulocorrelativo = correlativo;
                    $scope.titulonroCopia = nroCopia;
                    $scope.tituloasunto = asunto;
                    $scope.titulotipo_tramite = tipo;
                    $scope.btnImprimir = true;
                    break;
              case 2: // volver a bandeja de entrada
                    $scope.formActuacion = false;
                    $scope.formFormulario = false;
                    $scope.MostrarCopias = false;
					sessionService.destroy('TRAMITE_UID');
					sessionService.destroy('TRAMITE_IDNODO');
					sessionService.destroy('TRAMITE_NROCOPIA');
                    break;
        }
    };
    
    	$scope.cargarCopias=function(){
         $scope.formActuacion = false;
         $scope.formFormulario = true;
         $scope.ResultadoBusqueda = false;
         $scope.MostrarCopias = true;
		 $scope.filtros = false;
    };
    
    $scope.cargarBuscador=function(){
         $scope.formActuacion = false;
         $scope.formFormulario = true;
         $scope.ResultadoBusqueda = true;
         $scope.MostrarCopias = false;
		 $scope.filtros = false;
    };
    
        $scope.cargarNuevaBusqueda=function(){
         $scope.formFormulario = true;
         $scope.tablaDatos = false;
         $scope.formActuacion = false; 
         $scope.ResultadoBusqueda = false;
         $scope.MostrarCopias = false;
		 $scope.filtros = true;
    };
    
     $scope.startDateOpen = function($event) {
        //alert("entra hasta");
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
    };
    
        $scope.startDateOpen1 = function($event) {
            //alert("entra desde");
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened1 = true;
    };
    
        $scope.Recargar = function () {
            //alert("limpiar");
        sessionService.destroy('TRAMITE_UID');
		sessionService.destroy('TRAMITE_IDNODO');
		sessionService.destroy('TRAMITE_NROCOPIA');
        
        //var limpiar1 =  $scope.datos;
        //var limpiar2 = $scope.mytree;
            if($scope.datos != undefined) {$scope.datos = ''; }
            
            console.log($scope.mytree);
            if($scope.mytree.currentNode != undefined){$scope.mytree.currentNode.roleId = null;
                                       $scope.mytree.currentNode.roleName = '';}
        //$scope.arbolNodos();
    };
    /////////////////////////////////// ARBOL DERIVACION PARA NEXO //////////////////////////////////////

    $scope.jsonArbol = "";
    $scope.obtDatos = "";
    $scope.arbolNodos = function () {
      $.ajax({ 
        data:{ } ,            
        url: 'http://192.168.5.243:8080/sysworkflow/es/ae/gamlpOnLine/services/storeArbolAjax.php',
        type: 'post', 
        dataType: "json",
        success: function (response) { 
          $timeout(function () {
            var tempJsonArbol =JSON.stringify(response);
            
            tempJsonArbol = tempJsonArbol.replace(/desc_arbol/gi, 'roleName');
            tempJsonArbol = tempJsonArbol.replace(/id_arbol/gi, 'roleId');
            tempJsonArbol = tempJsonArbol.replace(/expanded/gi, 'collapsed');
            
            $scope.jsonArbol = JSON.parse(tempJsonArbol);
                        
            $scope.roleList = $scope.jsonArbol;//alert($scope.jsonArbol[0].roleId);
            //console.log($scope.jsonArbol);
          }, 1000);
        }
      });
    }
    
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////// HISTORICO PARA NEXO ///////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	

  	$scope.$on('api:ready',function(){
        $.blockUI();
        $scope.inicioNexo(); 
        $scope.formFormulario = true;
        $scope.formActuacion = false;
        $scope.MostrarCopias = false;
		$scope.filtros = true;
        $.unblockUI();
    });

    $scope.inicioNexoForm = function () {
        $.blockUI();
         $scope.formFormulario= true;
		 $scope.filtros = true;
         if(DreamFactory.isReady()){        	
            $scope.inicioNexo();
            //$scope.arbolNodos();
            $scope.formFormulario = true;
            
        }
         setTimeout(function(){ 
                $.unblockUI(); $scope.DesbloquearMenu();
            },1000);

    }; 
});
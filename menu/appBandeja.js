app.controller('bandejaController', function ($scope, $route,$rootScope, DreamFactory, CONFIG,sessionService,ngTableParams,$filter,sweet,$timeout) {
    var fecha= new Date();
    var fechactual=fecha.getFullYear() + "-" + fecha.getMonth() + "-" + fecha.getDate() + " " + fecha.getHours() + ":" + fecha.getMinutes() + ":" + fecha.getSeconds();
    var size = 10;
        console.log(sessionService.get('TRAMITE_NODO'));
        console.log(sessionService.get('TRAMITE_IDNODO'));
        console.log(sessionService.get('TRAMITE_NODO'));

    $scope.tablaBandeja = {};
    $scope.obtBandeja = "";
	$scope.templates =
    [
        { name: 'template0.html', url: '../../../app/view/salud/sldHistoricoConsultasMenu/index.html'},
        { name: 'template1.html', url: '../../../app/view/nexo/formularioMostrar/index.html'},
        { name: 'template2.html', url: '../../../app/view/nexo/actuaciones/index.html'},
        { name: 'template3.html', url: '../../../app/view/nexo/fusion/index.html'},
        { name: 'template4.html', url: '../../../app/view/nexo/historico/index.html'},
        { name: 'template3.html', url: '../../../app/view/nexo/derivar/index.html'}
    ];

    $scope.valor=function(opcion){
        $rootScope.verActuar=opcion;
        $rootScope.verTitulo=false;
    }
	$scope.cargar=function(cieId){
        if(cieId==1)
        {
            $rootScope.$broadcast('cargaFormulario', 'hola3');
        }
        $scope.template = $scope.templates[cieId];
    };

    $scope.startDateOpen = function($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.startDateOpened = true;
    };
    $scope.getNodo = function(){
        var resNodo = {
            "procedure_name":"sp_lst_nodo",
            "body":{"params": [{"name":"usuario","param_type":"IN","value":sessionService.get('USUARIO')}]}
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resNodo)
        .success(function (response) {
            $scope.nodos = response;
            $timeout( function(){ 
                $.unblockUI();
                $scope.DesbloquearMenu();
                $scope.getNodoSelect(); 
            }, 1000);
        }).error(function(error) {
             //$scope.errors["error_reg_civil"] = error;
             $timeout( function(){
                $.unblockUI();  $scope.DesbloquearMenu();
            },1000);
        });
    }
    $scope.getNodoSelect = function(){
        var valorCombo='';
        if(sessionService.get('TRAMITE_IDNODO')){
            valorCombo='{"nodoid":'+sessionService.get('TRAMITE_IDNODO')+',"nodonombre":"'+sessionService.get('TRAMITE_NODO')+'"}'; 
        }
        else{
            valorCombo='{"nodoid":'+sessionService.get('US_IDNODO')+',"nodonombre":"'+sessionService.get('US_NODODESCRIPCION')+'"}';    
        }
        if(document.getElementById('NodoId'))
        {
            document.getElementById('NodoId').value = valorCombo
        }
    }
    //listar Bandeja
    $scope.getBandeja = function(){
        console.log("mi usuario:" + $scope.usuario);
        $.blockUI(); 
		$scope.formActuacion = false;
        $scope.tableTramites = true;
        var resBandeja = {
            "procedure_name":"sp_lst_tramites_nodo", 
            "body":{
			    "params": [{
					"name": "nodoId",
					"value": sessionService.get('TRAMITE_IDNODO')
				},{
					"name": "estado",
					"value": 'ACTIVO'
				}]
			}
        };
        //servicio listar personas
        var obj=DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(resBandeja);
        obj.success(function (response) {
            $scope.obtBandeja = response;
            var data = response;   //grabamos la respuesta para el paginado
            $scope.tablaBandeja.reload();    
            setTimeout(function(){            
                $.unblockUI();  
                $scope.DesbloquearMenu();
            },1000);
        })
        obj.error(function(error) {
            //$scope.errors["error_Bandeja"] = error;

            $.unblockUI();  
            $scope.DesbloquearMenu();            
        });
    };
    $scope.tablaBandeja = new ngTableParams({
        page: 1,
        count: 10,
        filter: {},
        sorting: {}
    }, {
        total: $scope.obtBandeja.length,
        getData: function($defer, params) {
            var filteredData = params.filter() ?
            $filter('filter')($scope.obtBandeja, params.filter()) :
            $scope.obtBandeja;
            var orderedData = params.sorting() ?
            $filter('orderBy')(filteredData, params.orderBy()) :
            $scope.obtBandeja;
            params.total(orderedData.length);
            $defer.resolve(orderedData.slice((params.page() - 1) * params.count(), params.page() * params.count()));                                     
        }
    });
    $scope.filtroTramites = function(nodo){
        console.log(nodo);
        var nodoDato=JSON.parse(nodo);
        sessionService.set('TRAMITE_NODO', nodoDato.nodonombre);
        sessionService.set('TRAMITE_IDNODO', nodoDato.nodoid);
        $scope.getBandeja();
    };
    $scope.habilitaActuacion = function(opcion, index,idNodo, correlativo, nroCopia, asunto, tipo_tramite, tomado_por)
    {
        switch(opcion) {
            case 1: // mostrar Tramite
                $scope.formActuacion = true;
                $scope.tableTramites = false;
				sessionService.set('TRAMITE_UID', index);				
				sessionService.set('TRAMITE_NROCOPIA', nroCopia);
				if(nroCopia == 0){ nroCopia="Original";}
				$scope.titulocorrelativo = correlativo;
                $scope.titulonroCopia = nroCopia;
                $scope.tituloasunto = asunto;
                $scope.titulotipo_tramite = tipo_tramite;
            break;
            case 2: // volver a bandeja de entrada
                $scope.formActuacion = false;
                $scope.tableTramites = true;
				sessionService.destroy('TRAMITE_UID');
				//sessionService.destroy('TRAMITE_IDNODO');
				sessionService.destroy('TRAMITE_NROCOPIA');
                $route.reload();
            break;
        }
    };    

    $scope.tomarTramite = function (datos, opcion,opcion2, index,idNodo, correlativo, nroCopia, asunto, tipo_tramite, tomado_por) {
        console.log(datos.estado_paso);
        if(opcion == 'actuar')
        {
            sessionService.set('TRAMITE_US_ACTUAL', 'SI');
            $scope.usuario = sessionService.get('USUARIO');
        }
        else
        {
            sessionService.set('TRAMITE_US_ACTUAL', 'NO');
            $scope.usuario = '';
        }
        console.log(datos);
        var misDatos = {
            "procedure_name":"sp_tomar_tramite",
            "body":{
                "params": [
                    {"name": "uid","value": datos.uid},
                    {"name": "nro_copia","value": datos.nro_copia},
                    {"name": "usuario","value": $scope.usuario}
                ]
            }
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(misDatos).success(function (response){
            console.log("Response es:" + response[0].sp_tomar_tramite);
            if(response[0].sp_tomar_tramite){
                $scope.habilitaActuacion(opcion2, index,idNodo, correlativo, nroCopia, asunto, tipo_tramite, tomado_por);                
                $scope.cargar(1);
                $scope.valor('actuar');
                //$scope.getBandeja();
                /*if(opcion != 'actuar')
                {
                    $route.reload();
                }*/
            } else {
                sweet.show('', "El tramite ya fue tomado por otro Usuario", 'error');
            }
        }).error(function(response){
        });
    };

    $scope.recibirTramite = function (datos){
        var misDatos = {
            "procedure_name":"sp_recibir_tramite",
            "body":{
                "params": [
                    {
                        "name": "cuid",
                        "value": datos.uid
                    },
                    {
                        "name": "ncopia",
                        "value": datos.nro_copia
                    },
                    {
                        "name": "cruser",
                        "value": sessionService.get('USUARIO')
                    }
                ]
            }
        };
        DreamFactory.api[CONFIG.SERVICE].callStoredProcWithParams(misDatos).success(function (response){
            if(response[0].sp_recibir_tramite){
                $route.reload();
            } else {
                sweet.show('', "El registro no fue recibido", 'error');
            }
        }).error(function(response){
        });
    };

    //***************************MENU HISTORIAL ************************   
    //iniciando el controlador
    $scope.$on('api:ready',function(){
        $scope.usuario = sessionService.get('USUARIO');
        sessionService.set('TRAMITE_NODO', sessionService.get('US_NODODESCRIPCION'));
        sessionService.set('TRAMITE_IDNODO', sessionService.get('US_IDNODO'));
        $scope.getBandeja();
        $scope.getNodo();
    });
    $scope.inicioBandeja = function () {
        if(DreamFactory.isReady()){
            $scope.usuario = sessionService.get('USUARIO');
            sessionService.set('TRAMITE_NODO', sessionService.get('US_NODODESCRIPCION'));
            sessionService.set('TRAMITE_IDNODO', sessionService.get('US_IDNODO'));
            $scope.getBandeja();
            $scope.getNodo();
        }
    };
});